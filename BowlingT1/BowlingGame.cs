﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BowlingT1
{
    public class Jugador
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int Puntaje { get; set; }
    }

    public class Lanzamiento
    {
        public int Pinos { get; set; }
        public int JugadorId { get; set; }
    }

    public class BowlingGame
    {
        private List<Jugador> Jugadores = new List<Jugador>();
        private List<Lanzamiento> Lanzamientos = new List<Lanzamiento>();
        private int turno = 0;

        public object GetPuntajeJugador(int v)
        {
            throw new NotImplementedException();
        }

        public void Lanzar(int v)
        {
            Lanzamientos.Add(new Lanzamiento { Pinos = v, JugadorId = Jugadores.ElementAt(turno).Id });
        }

        public int[] GetPuntajeJugadores()
        {
            throw new NotImplementedException();
        }

        public void RegistrarJugador(Jugador jugador)
        {
            Jugadores.Add(jugador);
        }
    }
}
