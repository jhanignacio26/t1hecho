﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingT1
{
    public class CalcularImpuestos
    {
        private UIT uit { get; set; }
        private Quinta quinta { get; set; }

        public CalcularImpuestos()
        {
            uit = new UIT();
            quinta = new Quinta();
        }

        public decimal GetImpuestoQuintaCategoria(int monto)
        {
            var montoImpuesto = (monto - uit.GetUIT()) * quinta.GetQuinta();
            return montoImpuesto;
        }
    }
}
