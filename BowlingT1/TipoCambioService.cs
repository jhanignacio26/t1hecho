﻿using BowlingT1.Interfaces;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace BowlingT1
{
    public class TipoCambioService : ITipoCambioService
    {
        public decimal tipoDeCambio = 3.70m;

        public decimal GetTipoCambioDolaresSoles()
        {
            var task = Task.Run(async () => await Call());
            return task.Result;
        }

        private async Task<decimal> Call()
        {
            decimal value = 0;

            using (var httpClient = new HttpClient())
            {
                using (var response = await httpClient.GetAsync("https://run.mocky.io/v3/257513ee-94ff-4880-928c-7b486fbb9fbe"))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    value = JsonConvert.DeserializeObject<decimal>(apiResponse);
                }
            }
            return value;
        }
    }
}