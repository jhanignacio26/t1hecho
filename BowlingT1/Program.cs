﻿using System;

namespace BowlingT1
{
    class Program
    {
        static void Main(string[] args)
        {
            var c = new CalcularSalarioSoles(new TipoCambioService());
            Console.WriteLine("Ingrese monto en dolares");
            var d = int.Parse(Console.ReadLine());

            Console.WriteLine("Su monto en soles es " + c.Calcular(d));
        }
    }
}
