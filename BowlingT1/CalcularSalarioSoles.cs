﻿using BowlingT1.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingT1
{
    public class CalcularSalarioSoles
    {

        public ITipoCambioService service;

        public CalcularSalarioSoles(ITipoCambioService v)
        {
            service = v;
            
           // service.tipoDeCambio = v;
           // service.GetTipoCambioDolaresSoles() = v;
        }

        public decimal Calcular(decimal salarioDolares/*, decimal v*/)
        {
            //service.tipoDeCambio = v;
            // return salarioDolares * service.tipoDeCambio;
            //service.GetTipoCambioDolaresSoles() = 3.68;
            return salarioDolares * service.GetTipoCambioDolaresSoles();
        }

        //public void setTipoDeCambio(decimal v)
        //{
        //    service.tipoDeCambio = v;
        //}
    }
}
