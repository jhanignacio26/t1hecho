﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingT1.Interfaces
{
    public interface ITipoCambioService
    {
        decimal GetTipoCambioDolaresSoles();
    }
}
