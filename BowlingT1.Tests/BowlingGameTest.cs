using NUnit.Framework;

namespace BowlingT1.Tests
{
    [TestFixture]
    public class BowlingGameTest
    {
       
        [Test]
        public void Caso1()
        {
            var game = new BowlingGame();

            var jugador = new Jugador { Id = 1, Nombre = "Luis mendoza" };

            game.RegistrarJugador(new Jugador { Id = 1, Nombre = "Luis mendoza" });
            game.RegistrarJugador(new Jugador { Id = 2, Nombre = "Pepito Perez" });           

            game.Lanzar(1); // jugada del jugador 1
            game.Lanzar(1); // jugada del jugador 1
            game.Lanzar(1); // jugada del jugador 2
            game.Lanzar(1); // jugada del jugador 2

            var puntajeJugador1 = game.GetPuntajeJugador(1);
            var puntajeJugador2 = game.GetPuntajeJugador(2);

            Assert.AreEqual(2, puntajeJugador1);
            Assert.AreEqual(2, puntajeJugador2);

        }

        [Test]
        public void Caso2Spare()
        {
            var game = new BowlingGame();

            game.RegistrarJugador(new Jugador { Id = 1, Nombre = "Luis mendoza" });
            game.RegistrarJugador(new Jugador { Id = 2, Nombre = "Pepito Perez" });

            game.Lanzar(9); // jugada del jugador 1
            game.Lanzar(1); // jugada del jugador 1
            game.Lanzar(1); // jugada del jugador 2
            game.Lanzar(1); // jugada del jugador 2
            game.Lanzar(5); // jugada del jugador 2

            var puntajeJugador1 = game.GetPuntajeJugador(1);
            var puntajeJugador2 = game.GetPuntajeJugador(2);

            Assert.AreEqual(15, puntajeJugador1);
            Assert.AreEqual(2, puntajeJugador2);


        }

        [Test]
        public void Caso2Strike()
        {
            var game = new BowlingGame();

            game.RegistrarJugador(new Jugador { Id = 1, Nombre = "Luis mendoza" });
            game.RegistrarJugador(new Jugador { Id = 2, Nombre = "Pepito Perez" });

            game.Lanzar(10); // jugada del jugador 1
            game.Lanzar(5); // jugada del jugador 2
            game.Lanzar(5); // jugada del jugador 2
           
            game.Lanzar(5); // jugada del jugador 1
            game.Lanzar(3); // jugada del jugador 1
           
            game.Lanzar(3); // jugada del jugador 2

            var puntajeJugador1 = game.GetPuntajeJugador(1);
            var puntajeJugador2 = game.GetPuntajeJugador(2);

            Assert.AreEqual(26, puntajeJugador1);
            Assert.AreEqual(16, puntajeJugador2);

        }
    }
}