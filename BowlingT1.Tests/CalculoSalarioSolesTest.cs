﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using BowlingT1;
using BowlingT1.Interfaces;
using Moq;

namespace BowlingT1.Tests
{
    [TestFixture]
    public class CalculoSalarioSolesTest
    {
        [Test]
        public void Caso1()
        {
            var mockTipoDeCambio = new Mock<ITipoCambioService>();
            mockTipoDeCambio.Setup(o => o.GetTipoCambioDolaresSoles()).Returns(3.68m);
            var obj = mockTipoDeCambio.Object;
            
            //new TipoCambioService368();
            var calcular = new CalcularSalarioSoles(obj);
            //tipo de cambio sea 3.68;
            //calcular.setTipoDeCambio(3.68m);
            var sueldoSoles = calcular.Calcular(1);

            Assert.AreEqual(3.68, sueldoSoles);
        }

        [Test]
        public void Caso2()
        {
            var mockTipoDeCambio = new Mock<ITipoCambioService>();
            mockTipoDeCambio.Setup(o => o.GetTipoCambioDolaresSoles()).Returns(3.69m);
            var obj = mockTipoDeCambio.Object;

            var calcular = new CalcularSalarioSoles(obj);
            //tipo de cambio sea 3.69;
            //calcular.setTipoDeCambio(3.69m);
            var sueldoSoles = calcular.Calcular(1000);

            Assert.AreEqual(3690m, sueldoSoles);
        }

        [Test]
        public void Caso3()
        {
            var mockTipoDeCambio = new Mock<ITipoCambioService>();
            mockTipoDeCambio.Setup(o => o.GetTipoCambioDolaresSoles()).Returns(3.70m);
            var obj = mockTipoDeCambio.Object;

            var calcular = new CalcularSalarioSoles(obj);
            //tipo de cambio sea 3.70;
            //calcular.setTipoDeCambio(3.69m);
            var sueldoSoles = calcular.Calcular(1000);

            Assert.AreEqual(3700m, sueldoSoles);
        }
    }
}
